<pre>#!/usr/bin/python
"""Combines the whole javascript stack, and optionally minifies. Minification caused a bug, so it doesn't minify at all.
Required dependencies:
    - pip install slimit
	- pip install watchdog"""

from slimit import minify
from watchdog.events import FileSystemEventHandler
from watchdog.events import PatternMatchingEventHandler
from watchdog.observers import Observer
import argparse
import os
import platform
import sys
import time

base_dir = "src/"
patterns = ["*.js"]
files = ["lib/jquery.js",
      "lib/underscore-min.js",
      "lib/backbone-min.js",
      "lib/jquery-ui-custom.min.js",
	  "lib/json2.js",
      "lib/jquery.pagination.js",
      "lib/jquery.uniform.min.js",
      "lib/jquery.countdown.min.js",
      "lib/jquery.slider.js",
	  "lib/idle-timer.js",
      "core/util/util.js",
      "core/util/routerUtil.js",
      "core/util/dom.js",
      "models/*",
	  "collections/*",
	  "views/*",
      "core/router.js",
      "core/onDocumentReady.js"]

merged_file = "dist/ui.min.js"

# Someone will love me for this (or will ignore my warning).
# A comment about a comment.
header = """/**
 * THIS FILE WAS AUTOMATICALLY CREATED USING min.py which can be found at
 * src/main/webapp/static/js/min.py
 * All your changes will be removed.
 */"""

class CustomEventHandler(PatternMatchingEventHandler): 
	def on_modified(self, event):
		if(opts.debug):
			print event

		print "File change found. Beginning to minify...",
		combine(opts.minify)
		print "done."
		print
		
def combine(minify_output):
	"""
	This is where the main logic occurs.
	"""

	# append header to output
	output = header
	# get the output of all files
	files_content = ""
	for file in files : 
		file = base_dir + file
		# means we should include all files in the directory.
		if(file.endswith("*")) :
			dir_name = file[0:-1]
			for file in os.listdir(dir_name):
				files_content += "\n" + open(dir_name + file).read()
		else : 
			files_content += "\n" + open(file).read()

	# if necessary, minify
	output += minify(files_content, mangle=True) if minify_output else files_content
	end_file = open(merged_file, "w");
	end_file.write(output);
	end_file.close()

if __name__ == '__main__' :
	# parse command-line args
	parser = argparse.ArgumentParser(description='Combines the whole javascript stack, and optionally minifies.')
	parser.add_argument('--minify', action='store_true')
	parser.add_argument('--debug', action='store_true')
	opts = parser.parse_args()

	if(opts.debug):
		print "Debugging" 

	# configure the monitoring of JS files
	print "Configuring listener on '" + base_dir + "' for patterns '" + ("', '".join(patterns)) + "'"

	event_handler = CustomEventHandler(patterns=patterns)    
	observer = Observer()
	observer.schedule(event_handler, base_dir, recursive=True)
	observer.start()

	print "Initial minification...",
	combine(opts.minify)
	print "done"

	# loop
	try:
		while True:
			time.sleep(1)
	except KeyboardInterrupt:
		observer.stop()
	observer.join()
</pre>